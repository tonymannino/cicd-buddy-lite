<p align="center">
  <a href="#" target="_blank" rel="noopener noreferrer">
    <img width="180" src="docs/logo.png" alt="Vite logo">
  </a>
</p>
<br/>

# CICD Buddy *lite*

> A simple, quick and easy way to parameterize GitLab CI Pipelines

- Super quick installation
- Hosted on GitLab pages (private by default!)
- Fully configurable
- Supports cloud and private hosted GitLab
- 100% Open Source

[Demo website available here](https://tonymannino.gitlab.io/demo-cicd-buddy-lite)

[Demo Video available here](https://www.youtube.com/watch?v=76Rt3gAbph0)

## Getting Started

CICD Buddy *lite* is a simple configuration and UI that allows project maintainers to parameterize their GitLab CI pipelines and remotely trigger them from a simple and easy to use UI.

This tool is build around the [GitLab CI Pipeline Trigger API](https://docs.gitlab.com/ee/ci/triggers/) which utilizes individual project's [Pipeline Trigger Tokens](https://docs.gitlab.com/ee/ci/triggers/#create-a-trigger-token) which allow pipelines to start on behalf of a user.

Additionally the site can be configured to use a [Read Only Access Token]() to dynamically load `project branches` and past `pipelines`. For premium cloud and self-hosted users, a [Project Access Token](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html) can be created that is scoped only to the project that is running CICD Buddy lite. Regardless, use these tokens to your best discression, and always use `read only` tokens.

### How it works

GitLab allows pipelines to be remotely triggered with the following simple HTTP call:

```sh
# cURL example from https://docs.gitlab.com/ee/ci/triggers/#pass-cicd-variables-in-the-api-call
curl --request POST \
     --form token=TOKEN \
     --form ref=main \
     --form "variables[UPLOAD_TO_S3]=true" \
     "https://gitlab.example.com/api/v4/projects/123456/trigger/pipeline"
```

What this also means is we can make this same HTTP API call directly from our browser. CICD Buddy *lite* utilizes this to create a simple user interface on top of this API.

If you're interested in creating your own, you can dive into the code in [docs](./docs/).

## Installation

There are 2 components to CICD Buddy *Lite.* The first is creating the `cicd.config.json` with the metadata for your project and a `Pipeline Trigger Token` for the project. The second is importing the library into an existing `.gitlab-ci.yml` for deploying on GitLab pages.

### 1. Config File

Create a file named `cicd.config.json` in the root directory of your project. The only required parameters for this to work are `ProjectID` and a [PipelineTriggerToken](https://docs.gitlab.com/ee/ci/triggers/#create-a-trigger-token) for the target project.

Below is an example of a config file. [See for complete reference](#cicd-config-reference).
```json
{
    "Name": "Example",
    "Description": "This is an example CICD Project",
    "ProjectID": "",
    "PipelineTriggerToken": "",
    "AccessToken": "",
    "ProjectURL": "",
    "Params": [
        {
            "Name": "EXAMPLE",
            "Type": "list",
            "Value": "dev,preprod,prod"
        },
        {
            "Name": "REGION",
            "Type": "string",
            "Value": "us-west-2"
        },
        {
            "Name": "BUILD",
            "Type": "boolean",
            "Value": "true"
        }
    ]
}
```

---

If you have a use-case for an additional variable you'd like parameterized, create an issue or fork it and customize to your liking!

### 2. Edit GitLab CI Config

Add the following snippet to the beginning of your projects `.gitlab-ci.yml` file.

```
include:
  - project: tonymannino/cicd-buddy-lite
    ref: master
    file: .cicd-buddy-lite.yml
```

This imports the [.cicd-buddy-lite.yml](./.cicd-buddy-lite.yml) CI library that handles downloading and publishing the UI portion to GitLab pages.

**Note** If you project currently uses GitLab Pages for a website related to that project, you can always create a new repository for hosting CICD Buddy *lite*. So long that the config file has your target project's **ProjectID** and a [PipelineTriggerToken](https://docs.gitlab.com/ee/ci/triggers/#create-a-trigger-token) for the target project.

### 3. Deploy Pages

Once the previous two steps have been completed, navigate to `Project -> CI/CD -> Pipelines` and click `Run Pipeline` in the top right corner.

In the variables section, add the following:

| Parameter Type | Key | Value |
| ---- | --- | ----- |
| `Variable` | `DEPLOY_PAGES` | `true` |

Click `Run Pipeline`, and when it is completed, you can check the GitLab pages status in `Project -> Settings -> Pages`.

---

Anytime you need to make changes to your config, just re-run these steps to update the website.

## CICD Config Reference

| Name | Description | Required |
| ---- | ----------- | -------- |
| `Name` | Displayed on website |  |
| `Description` | Displayed on website | |
| `ProjectID` | API call to trigger pipeline | `yes` |
| `BaseAPIUrl` | API url for self-hosted GitLab instances | `depends` |
| `DefaultBranch` | Placeholder on website for `Branch` field | |
| `PipelineTriggerToken` | API call to trigger pipeline | `yes` |
| `AccessToken` | Load all branches from Project automatically | |
| `Params` | List of parameters to be configured for `.gitlab-ci.yml` | `depends` |


Param Model:
| Name | Description |
| ---- | ----------- |
| `Name` | The key of the CI variable for GitLab. Case-sensitive. |
| `Type` | Type of variable: `list`, `string`, `boolean` |
| `Value` | The value of the CI variable for GitLab.<br><br>`list` - Options delimited by `,` (csv) style. (dropdown) <br>`boolean` - Either `true` or `false` (checkbox) <br>`string` - Any text value (text input box) |

### Example config file

```json
{
    "Name": "Example",
    "Description": "This is an example CICD Project",
    "ProjectID": "",
    "PipelineTriggerToken": "",
    "AccessToken": "",
    "ProjectURL": "",
    "Params": [
        {
            "Name": "EXAMPLE",
            "Type": "list",
            "Value": "dev,preprod,prod"
        },
        {
            "Name": "REGION",
            "Type": "string",
            "Value": "us-west-2"
        },
        {
            "Name": "BUILD",
            "Type": "boolean",
            "Value": "true"
        }
    ]
}
```

## Advanced Installation

The steps listed above provide a best case focus on ease of use. This section covers alternative ways to install this.

### Self Hosting UI Assets

If you want to skip the CI library step for deploying the pages, you can copy the contents of [docs](./docs/) and paste them into your source project.

The assets from the `docs` repo can be hosted anywhere a static site can be hosted. The general focus of this project was to make it flexible to fit everyones unique needs.

If you're still using GitLab for hosting, you still must add a `.gitlab-ci.yml` step to tell GitLab to host the assets under GitLab pages. A simple CI stage is provided below:

```yml
# Example GitLab Pages YML

stages:
  - pages

pages:
  stage: pages
  script:
    - mv docs public
  artifacts:
    paths:
    - public
  only:
  - master
```
<sup><a href="https://docs.gitlab.com/ee/user/project/pages/getting_started/pages_from_scratch.html" target="_blank">GitLab Pages Reference</a></sup>

## Development

If you want to make edits to any piece of this, simply fork and make sure to update the following:

In your `.gitlab-ci.yml`:
```yml
include:
  - project: tonymannino/cicd-buddy-lite # <- Update this if you forked
    ref: {{Branch Name}} # <- Update this to your Branch
    file: .cicd-buddy-lite.yml

variables:
  - CICD_BUDDY_REF: '{{ Replace Me }}' # <- Add this if you created a new branch
  - CICD_BUDDY_PROJECT_ID: '{{ Replace Me }}' # <- Add and update if you forked
```

The `include` statement is our import statement to deploy the website. If you copied and pasted the `docs` repo and are updating the website components yourself, then you can ignore this.

Once you change something in your own fork or branch, you need to run a pipeline an ensure artifacts were published at the completion of the CI pipeline. Once the pipeline has "deployed" the artifacts, then the `include` statement can find the assets properly.

### Development cycle

1. Make changes and push to branch
2. Run CI to publish artifacts
3. In consuming project with `include` statement, update to have correct `CICD_BUDDY_REF` and `CICD_BUDDY_PROJECT_ID` variables set.

The `.cicd-buddy-lite.yml` project programmatically `cURL`'s the published job artifacts from the development pipeline that you ran that published the artifacts.

The example script is here:
```sh
curl --output artifacts.zip --header "PRIVATE-TOKEN: $ACCESS_TOKEN" -k -LO "https://gitlab.com/api/v4/projects/$CICD_BUDDY_PROJECT_ID/jobs/artifacts/$CICD_BUDDY_REF/download?job=pages&job_token=$CI_JOB_TOKEN"
```


## Background Context

This project is a slimmed down version of a project that we use internally for work. Instead of hosting a separate website per config, we have one centralized website connected to a datastore for parameters. As much as we would have liked to open source that, some measures had to be taken to ensure the system was flexible enough for people to get started without having to solve for everyone's use case.

This tool also exists as a CLI written in Go. If enough people express interest in this, we can try and open source that tool as well. If you made it this far and understand the core concepts of how this works, I urge you to try and make your own as well. The core functionality is fairly simple and if this tool fits the workflow of your CICD automation, it lends itself to being an invaluable asset.

2022 [Tony Mannino](https://www.spaghet.me)
