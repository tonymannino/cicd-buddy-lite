import GitLab from './gitlab.js';

const app = Vue.createApp({
    data() {
        return {
            theme: 'default',
            themeList: [
                'default', 'dark'
            ],
            client: null,
            BaseAPIUrl: null,
            config: null,

            SelectedBranch: '',
            SelectedBranchError: false,

            branches: null,
            initError: null,
            triggerError: null,
            params: null,
            isLoading: false,

            pipelines: []
        };
    },
    async mounted() {
        this.theme = window.localStorage.getItem('--cicd-buddy-theme') || 'default';

        this.isLoading = true;

        try {
            const configRes = await fetch('./config.json');
            this.config = await configRes.json();

            this.BaseAPIUrl = this.config.BaseAPIUrl || 'https://gitlab.com/api/v4';
            this.SelectedBranch = this.config.DefaultBranch || '';

            if (this.config.AccessToken) {
                this.client = GitLab(this.BaseAPIUrl, this.config.AccessToken);

                await this.loadBranches();
                await this.loadPipelines();
            } else {
                this.client = GitLab(this.BaseAPIUrl, null);
            }

            this.initParams();
        } catch (err) {
            this.initError = err;
        }

        this.isLoading = false;
    },
    methods: {
        async loadBranches() {
            try {
                const branches = await this.client.ProjectBranches(this.config.ProjectID);
                this.branches = branches.map(b => b.name);

                if (this.branches.length) {
                    this.SelectedBranch = this.config.DefaultBranch || this.branches[0];
                }
            } catch (err) {
                this.initError = err;
            }
        },
        async loadPipelines() {
            try {
                this.pipelines = await this.client.Pipelines(this.config.ProjectID);
            } catch (err) {
                this.initError = err;
            }
        },
        async handleTrigger() {
            if (!this.config.ProjectID) {
                this.triggerError = 'No project ID in config. Cannot trigger pipeline.';
                return;
            }

            if (!this.config.PipelineTriggerToken) {
                this.triggerError = 'No pipeline trigger token in config. Cannot trigger pipeline.';
                return;
            }

            if (!this.SelectedBranch) {
                this.SelectedBranchError = true;
                return;
            }

            this.isLoading = true;
            this.triggerError = null;
            this.SelectedBranchError = false;

            const params = this.getViewModelItems();

            try {
                const response = await this.client.TriggerPipeline(this.config.ProjectID, this.config.PipelineTriggerToken, this.SelectedBranch, params);

                if (response && response.web_url) {
                    this.pipelines.unshift(response);
                    window.open(response.web_url, '_blank');
                }
            } catch (err) {
                this.triggerError = err;
            }

            this.isLoading = false;
        },
        initParams() {
            let params = [];

            for (let param of this.config.Params) {
                if (this.isListField(param.Type)) {
                    const items = this.getItems(param.Value);

                    if (items.length) {
                        param.ViewModel = items[0];
                    }
                }

                if (this.isBoolField(param.Type)) {
                    param.ViewModel = param.Value.toLowerCase() === 'true' ? true : false;
                }

                if (this.isStringField(param.Type)) {
                    param.ViewModel = param.Value;
                }

                params.push(param);
            }

            this.params = params;
        },
        getViewModelItems() {
            const items = this.params.map((p) => `variables[${p.Name}]=${p.ViewModel}`);
            return items.join('&');
        },
        getItems(str) {
            return str.split(',');
        },
        isChecked(val) {
            return val.toLowerCase() === 'true' ? true : false;
        },
        isListField(type) {
            return type.toLowerCase() === 'list';
        },
        isBoolField(type) {
            return type.toLowerCase() === 'boolean';
        },
        isStringField(type) {
            return type.toLowerCase() === 'string';
        },
        date(d) {
            return new Date(d).toLocaleString();
        },
        // ---- Theme Stuff
        cacheTheme(ev) {
            window.localStorage.setItem('--cicd-buddy-theme', ev.target.value);
        }
    },
});

app.mount('#app');