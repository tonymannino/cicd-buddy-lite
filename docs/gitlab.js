
////////// HTTP Client ///////////

export default (BaseURI, accessToken) => ({

    async Project(id) { return await this._get(`/projects/${id}`); },
    async ProjectBranches(id) { return await this._get(`/projects/${id}/repository/branches`); },
    async Pipelines(id) { return await this._get(`/projects/${id}/repository/branches`); },
    async Pipeline(id) { return await this._get(`/projects/${id}/repository/branches`); },
    async PipelineRetry(id) { return await this._get(`/projects/${id}/repository/branches`); },
    async TriggerPipeline(id, triggerToken, branch, params) {
        const formData = `token=${triggerToken}&ref=${branch}${(params ? `&${params}` : '')}`;
        const headers = {
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
            'Content-Type': 'application/json',
            'Upgrade-Insecure-Requests': '1'
        }
        return await this._post(`/projects/${id}/trigger/pipeline?${formData}`, null, headers)
    },

    async _get(path) {
        const response = await fetch(`${BaseURI}${path}`, {
            method: 'GET',
            cache: 'no-cache',
            referrerPolicy: 'no-referrer',
            headers: this._headers()
        });
        return await response.json();
    },

    async _post(path, data, headers) {
        const response = await fetch(`${BaseURI}${path}`, {
            method: 'POST',
            cache: 'no-cache',
            body: JSON.stringify(data),
            headers: headers || this._headers()
        });

        return await response.json();
    },

    async _put(path, data) {
        const response = await fetch(`${BaseURI}${path}`, {
            method: 'PUT',
            cache: 'no-cache',
            referrerPolicy: 'no-referrer',
            body: JSON.stringify(data),
            headers: this._headers()
        });
        return await response.json();
    },

    async _del(path) {
        const response = await fetch(`${BaseURI}${path}`, {
            method: 'DELETE',
            cache: 'no-cache',
            referrerPolicy: 'no-referrer',
            headers: this._headers()
        })
        return await response.json();
    },

    _headers() {
        return { 'Authorization': `Bearer ${accessToken}` }
    }
})